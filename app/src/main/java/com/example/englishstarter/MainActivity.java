package com.example.englishstarter;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.englishstarter.lessons.LessonsActivity;

public class MainActivity extends AppCompatActivity {

    private int mLastLessonNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button startButton = (Button) findViewById(R.id.start_button);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LessonsActivity.start(MainActivity.this);
            }
        });

        Button continueButton = (Button) findViewById(R.id.continue_button);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alreadyHaveLastLesson()) {
                    startLastLesson();
                } else {
                    Toast.makeText(MainActivity.this, "К сожалению ещё ни один урок не был запущен =(", Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button exitButton = (Button) findViewById(R.id.exit_button);
        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        mLastLessonNumber = sharedPreferences.getInt("KEY_LESSON_NUMBER", 0);
    }

    private boolean alreadyHaveLastLesson() {
        return mLastLessonNumber != 0;
    }

    private void startLastLesson() {
        LessonsActivity.start(MainActivity.this, mLastLessonNumber);
    }
}
