package com.example.englishstarter.lessons;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.englishstarter.R;

public class LessonsListFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    public LessonsListFragment() {
        // Required empty public constructor
    }

    public static LessonsListFragment newInstance() {
        LessonsListFragment fragment = new LessonsListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_lessons_list, container, false);

        Button oneLessonButton = (Button) view.findViewById(R.id.lesson_one_btn);
        oneLessonButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLessonStarted(1);
            }
        });
        Button twoLessonButton = (Button) view.findViewById(R.id.lesson_two_btn);
        twoLessonButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLessonStarted(2);
            }
        });
        Button mainMenuButton = (Button) view.findViewById(R.id.main_menu_btn);
        mainMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onExitToMainMenu();
            }
        });

        return view;
    }

    private void onLessonStarted(int lessonNumber) {
        if (mListener != null) {
            mListener.onLessonStarted(lessonNumber);
        }
    }

    private void onExitToMainMenu() {
        if (mListener != null) {
            mListener.onExitClicked();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {

        void onExitClicked();

        void onLessonStarted(int lessonNumber);
    }
}
