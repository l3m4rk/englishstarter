package com.example.englishstarter.lessons;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.englishstarter.R;

public class FirstLessonFragment extends Fragment {

    private static final int COUNT_FOR_WIN = 3;
    private OnFragmentInteractionListener mListener;

    private EditText mFishInput;
    private EditText mFrogInput;
    private EditText mParrotInput;

    public FirstLessonFragment() {
        // Required empty public constructor
    }

    public static FirstLessonFragment newInstance() {
        FirstLessonFragment fragment = new FirstLessonFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_first_lesson, container, false);

        mFishInput = (EditText) view.findViewById(R.id.first_color_input);
        mFrogInput = (EditText) view.findViewById(R.id.second_color_input);
        mParrotInput = (EditText) view.findViewById(R.id.third_color_input);
        Button disenchantButton = (Button) view.findViewById(R.id.check);
        disenchantButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String fishAnswer = mFishInput.getText().toString().toLowerCase();
                final String frogAnswer = mFrogInput.getText().toString().toLowerCase();
                final String parrotAnswer = mParrotInput.getText().toString().toLowerCase();
                checkIfWin(fishAnswer, frogAnswer, parrotAnswer);
            }
        });
        return view;
    }

    private void checkIfWin(String fishAnswer, String frogAnswer, String parrotAnswer) {
        if (fishAnswer.isEmpty() || frogAnswer.isEmpty() || parrotAnswer.isEmpty()) return;

        int rightAnswersCount = 0;

        if (isFishAnswerCorrect(fishAnswer)) {
            rightAnswersCount++;
        } else {
            mFishInput.setText("");
        }
        if (isFrogAnswerCorrect(frogAnswer)) {
            rightAnswersCount++;
        } else {
            mFrogInput.setText("");
        }
        if (isParrotAnswerCorrect(parrotAnswer)) {
            rightAnswersCount++;
        } else {
            mParrotInput.setText("");
        }
        if (rightAnswersCount == COUNT_FOR_WIN) {
            Toast.makeText(getContext(), "Молодец! Все зверюшки расколдованы!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getContext(), "К сожалению, не всех зверюшек удалось расколдовать, попробуй еще раз!", Toast.LENGTH_LONG).show();
        }
    }

    private boolean isFishAnswerCorrect(String fishAnswer) {
        return fishAnswer.equals("swims") || fishAnswer.equals("floats");
    }

    private boolean isParrotAnswerCorrect(String parrotAnswer) {
        return parrotAnswer.equals("flies");
    }

    private boolean isFrogAnswerCorrect(String frogAnswer) {
        return frogAnswer.equals("jumps");
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
