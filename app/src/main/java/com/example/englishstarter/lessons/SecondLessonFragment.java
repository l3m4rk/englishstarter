package com.example.englishstarter.lessons;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.englishstarter.R;

public class SecondLessonFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    private EditText mFirstColorInput;
    private EditText mSecondColorInput;
    private EditText mThirdColorInput;
    private ImageView mFlag;

    public SecondLessonFragment() {
        // Required empty public constructor
    }

    public static SecondLessonFragment newInstance() {
        SecondLessonFragment fragment = new SecondLessonFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_second_lesson, container, false);

        mFirstColorInput = (EditText) view.findViewById(R.id.first_color_input);
        mSecondColorInput = (EditText) view.findViewById(R.id.second_color_input);
        mThirdColorInput = (EditText) view.findViewById(R.id.third_color_input);
        mFlag = (ImageView) view.findViewById(R.id.flag_view);

        Button checkButton = (Button) view.findViewById(R.id.check);
        checkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String firstColor = mFirstColorInput.getText().toString().toLowerCase();
                final String secondColor = mSecondColorInput.getText().toString().toLowerCase();
                final String thirdColor = mThirdColorInput.getText().toString().toLowerCase();
                checkColors(firstColor, secondColor, thirdColor);
            }
        });

        return view;
    }

    private void checkColors(String firstColor, String secondColor, String thirdColor) {
        if (firstColor.isEmpty() || secondColor.isEmpty() || thirdColor.isEmpty()) return;

        int correctAnswersCount = 0;

        if (firstColor.equals("white")) {
            correctAnswersCount++;
        } else {
            mFirstColorInput.setText("");
        }

        if (secondColor.equals("blue")) {
            correctAnswersCount++;
        } else {
            mSecondColorInput.setText("");
        }

        if (thirdColor.equals("red")) {
            correctAnswersCount++;
        } else {
            mThirdColorInput.setText("");
        }

        if (correctAnswersCount == 3) {
            mFlag.setVisibility(View.VISIBLE);
            Toast.makeText(getContext(), "Становится видимым флаг!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getContext(), "К сожалению, не всех цвета введены правильно, попробуй еще раз!", Toast.LENGTH_SHORT).show();
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
