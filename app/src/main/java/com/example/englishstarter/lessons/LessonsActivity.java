package com.example.englishstarter.lessons;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.example.englishstarter.R;

public class LessonsActivity extends AppCompatActivity
        implements LessonsListFragment.OnFragmentInteractionListener,
        FirstLessonFragment.OnFragmentInteractionListener,
        SecondLessonFragment.OnFragmentInteractionListener {

    private static final String EXTRA_LAST_LESSON_NUMBER = "EXTRA_LAST_LESSON_NUMBER";

    public static void start(Context context) {
        Intent starter = new Intent(context, LessonsActivity.class);
        context.startActivity(starter);
    }

    public static void start(Context context, int lastLessonNumber) {
        Intent starter = new Intent(context, LessonsActivity.class);
        starter.putExtra(EXTRA_LAST_LESSON_NUMBER, lastLessonNumber);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lessons);

        final int lastLessonNumber = getIntent().getIntExtra(EXTRA_LAST_LESSON_NUMBER, 0);
        if (lastLessonNumber != 0) {
            openLastLesson(lastLessonNumber);
        } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(android.R.id.content, LessonsListFragment.newInstance())
                    .commit();
        }
    }

    private void openLastLesson(int lastLessonNumber) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
        editor.putInt("KEY_LESSON_NUMBER", lastLessonNumber);
        editor.apply();

        Fragment lessonFragment = lastLessonNumber == 1 ? FirstLessonFragment.newInstance() : SecondLessonFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(android.R.id.content, lessonFragment)
                .commit();

    }

    private void openLesson(int lessonNumber) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
        editor.putInt("KEY_LESSON_NUMBER", lessonNumber);
        editor.apply();

        Fragment lessonFragment = lessonNumber == 1 ? FirstLessonFragment.newInstance() : SecondLessonFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(android.R.id.content, lessonFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onExitClicked() {
        finish();
    }

    @Override
    public void onLessonStarted(int lessonNumber) {
        openLesson(lessonNumber);
    }
}
